<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([
            [
                'title'=>'Php is awesome!',
                'slug'=>'php_is_awesome',
                'intro'=>'Php is awesome!',
                'text'=>'Php is awesome!',
            ],
            [
                'title'=>'JavaScript is awesome!',
                'slug'=>'javascript_is_awesome',
                'intro'=>'JavaScript is awesome!',
                'text'=>'JavaScript is awesome!',
            ],
            [
                'title'=>'Html is awesome!',
                'slug'=>'html_is_awesome',
                'intro'=>'Html is awesome!',
                'text'=>'Html is awesome!',
            ],
        ]);
    }
}
