<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrdersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders')->insert([
            [
                'customer_name'=>'Вася Пупкин',
                'email'=>'vasya_pupok@vasya.com',
                'phone'=>0660001100,
                'feedback'=>'Очень качественные чехлы!!',
            ],
            [
                'customer_name'=>'Витя Голова',
                'email'=>'vitia_golova@vitia.com',
                'phone'=>0630001100,
                'feedback'=>'Все супер. Спасибо!',
            ],
            [
                'customer_name'=>'Максим Рука',
                'email'=>'maksim_ruka@maksim.com',
                'phone'=>0730001100,
                'feedback'=>'Спасибо за крутой чехол!!!',
            ],
        ]);
    }
}
