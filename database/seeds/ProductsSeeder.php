<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\Product;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($x=0; $x < 10; $x++){
            App\Product::create([
                'title'=>'Product_'.$x,
                'slug'=>'Product_'.$x,
                'price'=>10 * $x,
                'description'=>'This product is number'.$x,
                'category_id'=>1
            ]);
        }
//        factory(App\Http\Requests\Product::class,15)->create();
    }
//        DB::table('products')->insert([
//            [
//                'title'=>'Чехол для Ipad',
//                'slug'=>'case_for_ipad',
//                'price'=>700,
//                'description'=>'Лучший продукт на рынке!',
//                'category_id'=>1,
//            ],
//            [
//                'title'=>'Чехол для Mac',
//                'slug'=>'case_for_mac',
//                'price'=>1200,
//                'description'=>'Лучший продукт на рынке!',
//                'category_id'=>1,
//            ],
//            [
//                'title'=>'Чехол для Iphone',
//                'slug'=>'case_for_iphone',
//                'price'=>500,
//                'description'=>'Лучший продукт на рынке!',
//                'category_id'=>1,
//            ],
//        ]);
//    }
}
