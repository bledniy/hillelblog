<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->insert([
            [
                'title'=>'Информация о нашей компании',
                'slug'=>'about_our_company',
                'intro'=>'Здесь вы можете увидеть всю самую свежую информацию!',
                'content'=>'У нас самые лучшие чехлы на рынке!',
            ],
            [
                'title'=>'Контакты',
                'slug'=>'contacts',
                'intro'=>'Здесь вы можете найти всю контактную информацию',
                'content'=>'Телефон: +380630001112',
            ],
        ]);
    }
}
