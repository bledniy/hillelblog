<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                'name'=>'Cases',
                'slug'=>'cases',
            ],
            [
                'name'=>'Phones',
                'slug'=>'phones',
            ],
            [
                'name'=>'Tablets',
                'slug'=>'tablets',
            ],
        ]);
    }
}
