@extends('layout.base')

@section('jumbotron')
    <h1>Create new blog post</h1>
@endsection
@section('content')
    <div class="col-md-6">
        <div class="alert-danger">
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </div>

        <form action="/posts" method="post">
            @csrf
            <div class="form-group">
                <label for="title">Title:</label>
                <input type="text" name="title" id="title" class="form-control">
            </div>
            <div class="form-group">
                <label for="slug">Slug:</label>
                <input type="text" name="slug" id="slug" class="form-control">
            </div>
            <div class="form-group">
                <label for="intro">Intro:</label>
                <input type="text" name="intro" id="intro" class="form-control">
            </div>
            <div class="form-group">
                <label for="text">Body:</label>
                <textarea  name="text" id="text" class="form-control"></textarea>
            </div>
            <div class="form-group">
                <button class="btn btn-success">Add</button>
            </div>
        </form>
    </div>
@endsection
