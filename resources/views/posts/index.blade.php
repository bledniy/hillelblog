@extends('layout.base')

@section('jumbotron')
<h1>{{$pageTitle}}</h1>
<p>{{$pageDescription}}</p>
    @if(Session::has('success')):
    <div class="alert alert-success" role="alert">
        New blog post created success!
    </div>
    @endif
@auth
    <a href="/posts/create" class="btn btn-success">Add Post</a>
@endauth
@endsection

@section('content')
    @foreach($posts as $post)
<div class="col-md-4">
    <div class="card mb-4 shadow-sm">
        <svg class="bd-placeholder-img card-img-top" width="100%" height="225" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Thumbnail"><title>Placeholder</title><rect width="100%" height="100%" fill="#55595c"/><text x="50%" y="50%" fill="#eceeef" dy=".3em">Thumbnail</text></svg>
        <div class="card-body">
            <p>{{$post->title}}</p>
            <p class="card-text">{{$post->intro}}</p>
            <p class="card-text">{{$post->text}}</p>
            <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                    <a href="/posts/{{$post->slug}}" class="btn btn-sm btn-outline-secondary">View</a>
                    @auth
                    <a href="/posts/{{$post->slug}}/edit" class="btn btn-sm btn-outline-secondary">Edit</a>
                    <form action="/posts/{{$post->slug}}" method="post">
                        @csrf
                        @method('delete')
                        <button class="btn btn-sm btn-outline-secondary">Delete</button>
                    </form>
                    @endauth
                </div>
                <small class="text-muted">9 mins</small>
            </div>
        </div>
    </div>
</div>

    @endforeach

@endsection
