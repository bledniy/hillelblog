@extends('layout.base')

@section('jumbotron')
    <h1>{{$post->title}}</h1>
    <p>{{$post->intro}}</p>
@endsection
@section('content')
    <div class="col-md-12">
        <p>{{$post->text}}</p>
    </div>
    <div class="col-md-12">
        <form action="/comment" method="post">
            @csrf
            @auth
            <input type="hidden" name="post_id" value="{{$post->id}}">
            <div class="form-group">
                <label>Your name:
                    <input type="text" name="first_name" class="form-control" value="{{Auth::user()->name}}">
                </label>
            </div>
            <div class="form-group">
                <label>Comment:
                    <textarea name="body" class="form-control"></textarea>
                </label>
            </div>
            <button class="btn btn-success">Post Comment</button>
                @endauth
        </form>
    </div>
    <div class="col-md-12">
        <div class="comments">
            <ul class="list-group">
                @foreach($post->comments as $comment)
                    <li>{{$comment->first_name}} - {{$comment->body}} [{{$comment->created_at->diffForHumans()}}]</li>
                @endforeach
            </ul>
        </div>
    </div>
@endsection
