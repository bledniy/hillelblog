@extends('layout.base')

@section('jumbotron')
    <h1>Update blog post</h1>
@endsection
@section('content')
    <div class="col-md-6">
        <div class="alert-danger">
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </div>

        <form action="/posts/{{$post->slug}}" method="post">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="title">Title:</label>
                <input type="text" name="title" id="title" value="{{$post->title}}" class="form-control">
            </div>
            <div class="form-group">
                <label for="slug">Slug:</label>
                <input type="text" name="slug" id="slug" class="form-control" value="{{$post->slug}}">
            </div>
            <div class="form-group">
                <label for="intro">Intro:</label>
                <input type="text" name="intro" id="intro" value="{{$post->intro}}" class="form-control">
            </div>
            <div class="form-group">
                <label for="text">Body:</label>
                <textarea  name="text" id="text"  class="form-control">{{$post->text}}</textarea>
            </div>
            <div class="form-group">
                <button class="btn btn-success">Update</button>
            </div>
        </form>
    </div>
@endsection
