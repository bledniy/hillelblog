@extends('layout.base')

@section('jumbotron')
    <h1>Cart:</h1>

@endsection

@section('content')
    <div class="col-md-12">
        <table class="table">
            <tr>
                <th>ID</th>
                <th>Product</th>
                <th>Price</th>
                <th>Amount</th>
                <th>Total Price</th>
            </tr>
            @forelse($cart as $productData)
                <tr>
                    <td>{{$productData['product']->id}}</td>
                    <td>{{$productData['product']->title}}</td>
                    <td>{{$productData['product']->price}}</td>
                    <td>{{$productData['amount']}}</td>
                    <td>{{$productData['product']->countTotal($productData['amount'])}}</td>
                </tr>
                @empty
                <tr>
                    <td colspan="5">CART IS EMPTY</td>
                </tr>
                @endforelse
        </table>
        <div>
            <form action="/order" method="post">
                @csrf
                <div class="form-group">
                    <label>Full name:
                        <input type="text" name="customer_name" value="{{Auth::user()->name}}" class="form-control">
                    </label>
                    <label>Email:
                        <input type="email" name="email" id="email" value="{{Auth::user()->email}}" class="form-control">
                    </label>
                    <label>Phone number:
                        <input type="tel" name="phone" value="380" class="form-control">
                    </label>
                    <div>Feedback:
                        <textarea name="feedback" class="form-control"></textarea>
                    </div>
                </div>
                <button class="btn btn-success">Create order!</button>
            </form>
        </div>
    </div>
@endsection
