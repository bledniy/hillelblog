@extends('layout.base')

@section('jumbotron')
    <h1>Add new product</h1>
@endsection
@section('content')
    <div class="col-md-6">
        @include('partials.errors')
        <form action="/category/create">
            <div class="form-group">
                <button class="btn btn-success">Add new category</button>
            </div>
        </form>
        <form action="/products" method="post">
            @csrf
            <div class="form-group">
                <label for="title">Title:</label>
                <input type="text" name="title" id="title" class="form-control">
            </div>
                <label for="category_id">Выберите категорию:
                    <select name = 'category_id'>
                        @foreach($categories as $category)
                        <option name="category_id" id="category_id" value="{{$category->id}}">{{$category->name}}</option>
                        @endforeach
                    </select>
                </label>
            <div class="form-group">
                <label for="slug">Slug:</label>
                <input type="text" name="slug" id="slug" class="form-control">
            </div>

            <div class="form-group">
                <label for="price">Price:</label>
                <input type="number" name="price" id="price" class="form-control">
            </div>
            <div class="form-group">
                <label for="description">Description:</label>
                <textarea  name="description" id="description" class="form-control"></textarea>
            </div>
            <div class="form-group">
                <button class="btn btn-success">Add</button>
            </div>
        </form>
    </div>
@endsection
