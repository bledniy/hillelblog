@extends('layout.base')

@section('jumbotron')
    <h1>Sign In:</h1>
@endsection

@section('content')
    <div class="col-md-6">

        @include('partials.errors')

        <form action="/sign-in" method="post">

            @csrf

            <div class="form-group">
                <label for="email">Email:</label>
                <input type="text" name="email" id="email" value="{{ old('email') }}" class="form-control">
            </div>

            <div class="form-group">
                <label for="password">Password:</label>
                <input type="password" name="password" id="password" value="{{ old('password') }}" class="form-control">
            </div>

            <div class="form-group">
                <button class="btn btn-success">Sign in</button>
            </div>
        </form>
        <div class="form-group">
             <a href="{{$gitUrl}}"><img src="/resources/images/icons/gitIcon.png" alt="gitIcon"></a><a href="{{$googleUrl}}"><img src="/resources/images/icons/google-logo.png" alt="googleIcon"></a>
        </div>
    </div>
@endsection
