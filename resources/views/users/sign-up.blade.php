@extends('layout.base')

@section('jumbotron')
    <h1>Sign Up</h1>
@endsection
@section('content')
    <div class="col-md-6">
        <div class="alert-danger">
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </div>

        <form action="/sign-up" method="post">
            @csrf
            <div class="form-group">
                <label for="name">Name:</label>
                <input type="text" name="name" id="name" class="form-control">
            </div>
            <div class="form-group">
                <label for="email">Email:</label>
                <input type="text" name="email" id="email" class="form-control">
            </div>
            <label for="category_id">Admin:
                <select name = 'admin'>
                    <option name="admin" id="admin" value="1">Yes</option>
                    <option name="admin" id="admin" value="0">No</option>
                </select>
            </label>
            <div class="form-group">
                <label for="password">Password:</label>
                <input type="password" name="password" id="password" class="form-control">
            </div>
            <div class="form-group">
                <label for="password_conformation">Password Conformation:</label>
                <input type="password" name="password_conformation" id="password_conformation" class="form-control">
            </div>
            <div class="form-group">
                <button class="btn btn-success">Sign up</button>
            </div>
        </form>
    </div>
@endsection
