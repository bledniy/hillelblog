@extends('layout.base')

@section('jumbotron')
    <h1>Super Home page</h1>
    <div class="form-group">
    <a href="products" class="btn btn-outline-info">Товары</a>
    <a href="/posts" class="btn btn-outline-info">Посты</a>
    <a href="/order" class="btn btn-outline-info">Заказы</a>
    <a href="/pages" class="btn btn-outline-info">Странички</a>
    </div>
@endsection
@section('content')
    <div class="col-md-12">
        <h2>Hi I am homepage content</h2>
        <p>Some text here</p>
    </div>
@endsection

