@extends('layout.base')

@section('jumbotron')
    <h1>Add new product</h1>
@endsection
@section('content')
    <div class="col-md-6">
        <div class="alert-danger">
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </div>
        <form action="/category/create">
            <div class="form-group">
                <button class="btn btn-success">Add new category</button>
            </div>
        </form>
        <form action="/category/products" method="post">
            @csrf
            <div class="form-group">
                <label for="name">Category name:</label>
                <input type="text" name="name" id="name" class="form-control">
            </div>
            <div class="form-group">
                <label for="slug">Slug:</label>
                <input type="text" name="slug" id="slug" class="form-control">
            </div>
            <div class="form-group">
                <button class="btn btn-success">Add</button>
            </div>
        </form>
    </div>
@endsection
