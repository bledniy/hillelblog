@extends('layout.base')

@section('jumbotron')
    <h2>Pages</h2>

        @foreach($pages as $page)
            <div>
                <p>
                    <a href="/pages/{{$page->slug}}" class="btn btn-outline-info">{{$page->title}} </a>
                    <a href="/pages/{{$page->slug}}/edit" class="btn btn-sm btn-outline-secondary">Edit</a>
                <form action="/pages/{{$page->slug}}" method="post">
                    @csrf
                    @method('delete')
                    <button class="btn btn-sm btn-outline-danger">Delete</button>
                </form>
                </p>
            </div>
        @endforeach
    <a href="/pages/create" class="btn btn-success">Add Page</a>
@endsection

@section('content')




@endsection
