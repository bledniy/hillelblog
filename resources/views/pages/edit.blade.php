@extends('layout.base')

@section('jumbotron')
    <h1>Update page</h1>
@endsection
@section('content')
    <div class="col-md-6">
        <div class="alert-danger">
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </div>

        <form action="/pages/{{$pages->id}}" method="post">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="title">Title:</label>
                <input type="text" name="title" id="title" class="form-control" value="{{$pages->title}}">
            </div>
            <div class="form-group">
                <label for="slug">Slug:</label>
                <input type="text" name="slug" id="slug" class="form-control" value="{{$pages->slug}}">
            </div>
            <div class="form-group">
                <label for="intro">Intro:</label>
                <input type="text" name="intro" id="intro" class="form-control" value="{{$pages->intro}}">
            </div>
            <div class="form-group">
                <label for="content">Content:</label>
                <textarea  name="content" id="content" class="form-control">{{$pages->content}}</textarea>
            </div>
            <div class="form-group">
                <button class="btn btn-success">Update</button>
            </div>
        </form>
    </div>
@endsection

