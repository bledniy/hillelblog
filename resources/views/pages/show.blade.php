@extends('layout.base')

@section('jumbotron')
    <h1>{{$pages->title}}</h1>
@endsection
@section('content')
    <div class="col-md-12">
        <h2>{{$pages->slug}}</h2>
        <h3>{{$pages->intro}}</h3>
        <h5>{{$pages->content}}</h5>
    </div>

@endsection
