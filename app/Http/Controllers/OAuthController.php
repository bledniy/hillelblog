<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OAuthController extends Controller
{
    public function oauthGit(){

        $parameters = [
            'client_id' => getenv('GIT_CLIENT_ID'),
            'client_secret' => getenv('GIT_CLIENT_SECRET'),
            'code' => $_GET['code'],
            'redirect_uri' => getenv('GIT_REDIRECT_URI'),
        ];

        $client = new Client();
        $response = $client->post(
            'https://github.com/login/oauth/access_token',
            [
                'form_params'=> $parameters
            ]
        );

        parse_str($response->getBody()->getContents(), $result);

        $responseUser = $client->get('https://api.github.com/user',[
            'headers' => [
                'Authorization' => 'token ' . $result['access_token'],
            ],
        ]);

        $responseUserArr = json_decode($responseUser->getBody()->getContents(), true);

        $responseMail = $client->get('https://api.github.com/user/emails',[
            'headers' => [
                'Authorization' => 'token ' . $result['access_token'],
            ],
        ]);

        $responseMailArr = json_decode($responseMail->getBody()->getContents(), true);
        $userName = $responseUserArr['login'];
        $userMail = $responseMailArr[0]['email'];

        $user = User::where('email', $userMail)->first();

        if(isset($user->id))
        {
            Auth::login($user);

            return redirect('/');
        }else{

            $user = User::create(['name' => $userName, 'email' => $userMail] + ['password' => bcrypt(rand(10,1000))]);



            Auth::login($user);

            return redirect('/');
        }
    }
    public function oauthGoogle(){
        $parameters = [
            'client_id' => getenv('GOOGLE_CLIENT_ID'),
            'client_secret' => getenv('GOOGLE_CLIENT_SECRET'),
            'code' => $_GET['code'],
            'grant_type'    => 'authorization_code',
            'redirect_uri' => getenv('GOOGLE_REDIRECT_URI'),
        ];
        $client = new Client();

        $response = $client->post('https://accounts.google.com/o/oauth2/token',
            [
                'form_params' => $parameters,
            ]);


        $tokenInfo = json_decode($response->getBody()->getContents(), true);

        $parameters['access_token'] = $tokenInfo['access_token'];

        $userInfo = json_decode(file_get_contents('https://www.googleapis.com/oauth2/v1/userinfo' . '?' . urldecode(http_build_query($parameters))), true);

        $userName = $userInfo['name'];
        $userMail = $userInfo['email'];

        $user = User::where('email', $userMail)->first();

        if(isset($user->id))
        {
            Auth::login($user);

            return redirect('/');
        }else{

            $user = User::create(['name' => $userName, 'email' => $userMail] + ['password' => bcrypt(rand(10,1000))]);

            $user->groups()->attach(2);

            Auth::login($user);

            return redirect('/');
        }
    }
}
