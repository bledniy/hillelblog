<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }
    public function signUp(){
        return view('users.sign-up');
    }
    public function store(Request $request){
//        dd($request->all(['email','name'] )+ ['password'=>bcrypt($request->post('password'))]);
        $user = User::create($request->all(['email','name'] )+ ['password'=>bcrypt($request->post('password'))]);
        Auth::login($user);
        return redirect('/');
    }

    public function logout(){
        if (Auth::check()){
            Auth::logout();
        }
        return redirect('/');
    }
    public function signIn(){
        return view('users.sign-in');
    }
}
