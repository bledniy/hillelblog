<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{


    public function index(){

        echo 'Super secret dashboard';
    }

    public function secret(){
        return 'one more secret';
    }
}
