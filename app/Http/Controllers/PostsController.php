<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\Post\StorePostRequest;
use App\Http\Requests\Post\UpdatePostRequest;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostsController extends Controller
{
    public function index() {
        $posts = Post::all();
        $pageTitle = 'Posts listing page!';
        $pageDescription = 'Here you can view all our posts!';

        return view('posts.index')->with(compact('posts','pageTitle','pageDescription'));
    }
    public function show(Post $post){
        return view('posts.show')->with(compact('post'));
    }
    public function create(){
        return view('posts.create');
    }
    public function store(Post $post, StorePostRequest $request){

        Post::create($request->all());
        return redirect('/posts')->with('success','Blog post created!');
    }
    public function edit(Post $post){
        return view('posts.edit', compact('post'));
    }
    public function update(Post $post, UpdatePostRequest $request){
        $post->update($request->all());
        return redirect('/posts');

    }
    public function destroy(Post $post){
        $post->delete();
        return(redirect('/posts'));
    }
}
