<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\SignInUserRequest;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{

    public function __construct()
    {
        $this->middleware('guest', ['except' =>'destroy']);
        $this->middleware('auth',['only' => 'destroy']);
    }

    public function destroy(){
        if (Auth::check()){
            Auth::logout();
        }
        return redirect('/');
    }


    public function create(){
        $googleParameters = [
            'client_id' => getenv('GOOGLE_CLIENT_ID'),
            'redirect_uri' => getenv('GOOGLE_REDIRECT_URI'),
            'response_type' => 'code',
            'scope' => 'https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile',
        ];
        $gitParameters = [
            'client_id' => getenv('GIT_CLIENT_ID'),
            'redirect_uri' => getenv('GIT_REDIRECT_URI'),
            'scope' => 'read:user,user:email',
        ];
        $gitUrl = 'https://github.com/login/oauth/authorize?'.http_build_query($gitParameters);
        $googleUrl = 'https://accounts.google.com/o/oauth2/auth?' . http_build_query($googleParameters);
        return view('users.sign-in', compact('gitUrl','googleUrl'));
    }

    public function store(SignInUserRequest $request){
        if(Auth::attempt($request->all(['email', 'password']))) {
            return redirect('/');
        }else{
            return back();
        }
    }


}
