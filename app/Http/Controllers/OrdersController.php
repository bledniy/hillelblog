<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrdersController extends Controller
{
    public function index()
    {
        $orders = Order::all();
        return view('order.index')->with(compact('orders'));
    }

    public function create(){
        $cart = Cart::getCartWithProducts();

        return view('order.create',compact('cart'));
    }
    public function store(Request $request){
        $order = Order::create([
            'customer_name'=> $request->customer_name,
            'email'=> $request->email,
            'phone'=> $request->phone,
            'feedback'=> $request->feedback,
            'user_id'=> Auth::user()->id
        ]);
        $cart = Cart::getCartArray();
        foreach ($cart as $productId => $amount ){
            $order->products()->attach($productId,['amount'=>$amount]);
        }
        return redirect('/products')->withCookie('cart',null);
    }
}
