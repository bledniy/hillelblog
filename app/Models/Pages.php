<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

class Pages extends Model
{
    protected $fillable = ['title','slug','intro','content'];

    public function getRouteKeyName()
    {
        return 'slug';
    }
}
