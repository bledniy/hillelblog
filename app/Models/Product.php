<?php


namespace App;


use App\Models\Category;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['title','slug','price','description','category_id'];
    /**
     * @var mixed
     */

    public function getRouteKeyName()
    {
        return 'slug';
    }
    public function category(){
        return $this->belongsTo('App\Models\Category','category_id','id');
    }
    public function countTotal($amount){
        return round($this->price * $amount);
    }

}
