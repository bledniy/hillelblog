<?php

namespace App\Models;

use App\Product;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name','slug','product_id'];

    public function getRouteKeyName()
    {
        return 'slug';
    }
    public function ProductCategory(){
        return $this->hasMany(Product::class);
    }
}
