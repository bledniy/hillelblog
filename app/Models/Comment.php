<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['post_id','first_name','body'];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
