<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');

Route::get('/check', function (){
    return view('layout.base');
});

//Route::get('/posts', "PostsController@index");
//Route::get('/posts/create', "PostsController@create");
//Route::get('/posts/{post}', "PostsController@show");
//Route::post('/posts',"PostsController@store");
//Route::get('/posts/{post}/edit', "PostsController@edit");
//Route::put('/posts/{post}','PostsController@update');
//Route::delete('/posts/{post}', "PostsController@destroy");

Route::resource('posts', "PostsController");

Route::resource('products', "ProductsController", ['except' => [
    'show'
]]);
Route::get("/{product}/{category}", "ProductsController@show");

Route::middleware('auth')->group(function (){
    Route::get('/order',"OrdersController@create");
    Route::post('/order',"OrdersController@store");
});


Route::resource('pages',"PagesController");


Route::get('/sign-up',"UsersController@signUp");
Route::post('/sign-up',"UsersController@store");


//http://hillel-blog.test/oauth-git?code=d8cb329266e7c085257d
//http://hillelblog.test/sign-in

Route::delete('/logout',"LoginController@destroy");
Route::get('/sign-in',"LoginController@create")->name('login');
Route::post('/sign-in',"LoginController@store");

Route::get('/oauth-git', "OAuthController@oauthGit");
Route::get('/oauth-google', "OAuthController@oauthGoogle");

Route::post('/comment', "CommentController@store");

Route::resource('category/products',"CategoryController");

Route::get('/cart',"CartController@index");
Route::post('/cart/{product}',"CartController@store");


Route::get('/admin/dashboard',"Admin\DashboardController@index");

Route::prefix('admin')->middleware('admin')->group(function (){

    Route::get('dashboard',"Admin\DashboardController@index");
    Route::get('secret',"Admin\DashboardController@secret");
});
